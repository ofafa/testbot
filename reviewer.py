'''This program is designed for checking remote date and comparing the results with test case'''


class Reviewer:

    def __init__(self):
        '''
        >>> from slotter import Slotter, Identity, Operation
        >>> slotmachine = Slotter(20,0,'TW','rand',allowed_operation=('AP','RD'))
        >>> slotmachine.slot(50)
        >>> slotmachine.export()
        >>> reviewer            = Reviewer()
        Hello this is reviewer
        >>> userlist            = reviewer.import_testcase(userstatfile='userstat.txt')
        >>> userdata, userstat  = reviewer.fetch_remote_user_stat(userlist)
        >>> passcount, passrate = reviewer.check(userlist, remotedata)
        >>> passcount
        >>> passrate
        '''

        from   slotter import Slotter
        from   slotter import Identity

        print 'Hello this is reviewer'
        self._userlist = dict()

    def import_testcase(self, userlist=None, userstatfile=None):
        from slotter import Identity
        alluserlist = dict()
        if userlist is not None:
            alluserlist = userlist
        if userstatfile is not None:
            userstat = open(userstatfile, 'r')
            for line in userstat:
                row = line.split(',')
                user_identity   = row[0]
                user_binding    = row[1]
                user_point      = row[2]
                user_identifier = 'TAG'
                if user_identity[0:2] == '09' and len(user_identity) == 10:
                    user_identifier = 'PHONE'
                thisuser = Identity(identifier=user_identifier, \
                        value=user_identity, point=user_point)
                alluserlist[user_identity] = thisuser
        print '---All users---'
        print alluserlist
        return alluserlist

    def fetch_remote_user_stat(self, userlist=None\
            , url='http://t.api.stampex.sharelike.asia/stampa'):
        import requests
        import json
        from slotter import Identity
        remote_user_data = dict()
        remote_user_stat = dict()
        if userlist is None:
            userlist = self._userlist
        for user in userlist.values():
            header = ''
            if user._identifier is 'PHONE' and user._location is 'TW':
                header='%2B886' 
            query = '?customer[%s]=%s%s'\
                    %(user._identifier.lower(), header, user._value)
            result = requests.get(url + query)
            print 'Request: %s%s'%(url,query)
            print 'Remote data:'
            print result
            remote_user_data[user._value] = result.json()
            #remote_user_stat[user._value] = Identity(identity=user._identifier,\
            #        value=user._value, point=result.json()['points'])
        return remote_user_data, remote_user_stat

    def check(testcase, remotedata, checkitem={'POINT','LEVEL','BINDING'}):
        point_pass_count   = 0
        level_pass_count   = 0
        binding_pass_count = 0
        all_data_count     = len(testcase)
        if len(testcase) != len(remotedata):
            print 'Exception: testcase has different size with remotedata!'
            all_data_count = max(len(testcase), len(remotedate))

        for key in testcase.keys():
            if 'POINT' in checkitem:
                if testcase[key]._point == remotedata[key]._point:
                    point_pass_count += 1
            if 'LEVEL' in checkitem:
                pass
            if 'BINDING' in checkitem:
                if testcase[key]._binding == remotedata[key]._binding:
                    binding_pass_count += 1
        result_pass_count = 'P: %d, L: %d, B:%d'%(point_pass_count, level_pass_count\
                , binding_pass_count)
        result_pass_rate  = 'P: %.2f, L: %.2f, B: %.2f' \
                %(point_pass_count*1.0/all_data_size,   \
                  level_pass_count*1.0/all_data_size,   \
                  binding_pass_count*1.0/all_data_size  \
                  )
        return result_pass_count, result_pass_rate

if __name__ == '__main__':
    import doctest
    doctest.testmod()
