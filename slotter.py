'''This class is designed for generating randomed test cases with the following format:'''

'''Identity, Operation, Content'''
'''090000000, AddPoint, 5      '''
'''090000001, BindCard, 0x123  '''
'''0x123    , UnbindPhone, 0900000001'''
import string
import random


class Slotter:
    MIN_TAGID=3
    MAX_TAGID=8

    def __init__(self, size_phone_user=10, size_tag_user=10,location='TW',\
                 mode='rand', allowed_operation=('AP','RD','BP','BC'), \
                 test_case_file='testcase.txt', user_stat_file='userstat.txt'):
        '''
        >>> slot = Slotter(20,10,'TW','rand')
        >>> slot.slot(50)
        >>> slot
        >>> slot.export('testcase.txt', 'userstat.txt')
        '''
        print 'Hello this is Stampa slotter'
        #userlist[Identity._value] = Identity
        self._userlist = dict()
        self.genusers(self._userlist, size_phone_user, size_tag_user, location, mode)
        #operationlist[Opration.__repr__] = Operation
        self._allowed_operation = allowed_operation
        #slotcontainer {(Identity, Operation, Content),*}
        self._slotresults = list()

        self._test_case_file = test_case_file
        self._user_stat_file = user_stat_file

    def __repr__(self):
        for item in self._slotresults:
            print item
        return 'Finished'

    def export(self, filename_case=None, filename_stat=None):
        outfile_case = None
        if filename_case is None:
            outfile_case = open(self._test_case_file, 'w')
        else:
            outfile_case = open(filename_case, 'w')
        for slot in self._slotresults:
            outfile_case.write(('%s, %s ,%s\n')%(slot[0], slot[1], slot[2])) 
        outfile_case.close()
        
        outfile_stat = None
        if filename_stat is None:
            outfile_stat = open(self._user_stat_file, 'w')
        else:
            outfile_stat = open(filename_stat, 'w')
        for user in self._userlist.values():
            binding = None
            if user._binding is None:
                binding = 'None'
            else:
                binding = user._binding._value
            outfile_stat.write(('%s, %s, %s\n') %(user._value, binding, user._point))
        outfile_stat.close()
        return

    def genusers(self, userlist, size_phone_user, size_tag_user, location, mode):
        for i in range(size_phone_user):
            if mode is 'rand':
                he = Identity('PHONE', 8, location, mode)
                he.born()
                userlist[he._value] = he
            else:
                he = Identity('PHONE', i, location, mode=mode)
                he.born()
                userlist[he._value] = he
        for i in range(size_tag_user):
            if mode is 'rand':
                he = Identity('TAG', random.randint(Slotter.MIN_TAGID,\
                                     Slotter.MAX_TAGID), location, mode)
                he.born()
                userlist[he._value] = he
            else:
                he = Identity('TAG', random.randint(1,8), location, mode)
                he.born()
                userlist[he._value] = he




    def slot(self,size):
        '''generate random test $size cases'''
        for i in range(size):
            user_id = random.choice(self._userlist.keys())
            user    = self._userlist[user_id]
            op, self._userlist[user_id]\
                    = self.get_operation(user)
            self._slotresults.append((user_id, op._content_type, op._content))

    def get_operation(self, user):
        allowed_op = list(self._allowed_operation)
        if user._point is 0 and 'RD' in allowed_op:
            allowed_op.remove('RD')
        if user._binding is not None:
            if 'BC' in allowed_op:
                allowed_op.remove('BC')
            if 'BP' in allowed_op:
                allowed_op.remove('BP')
            if 'UC' in allowed_op and user._identifier is 'TAG':
                allowed_op.remove('UC')
            if 'UP' in allowed_op and user._identifier is 'PHONE':
                allowed_op.remove('UP')
        elif user._binding is None and len(self._userlist) is not 0:
            if 'UC' in allowed_op:
                allowed_op.remove('UC')
            if 'UP' in allowed_op:
                allowed_op.remove('UP')
            if 'BP' in allowed_op and user._identifier is 'PHONE':
                allowed_op.remove('BP')
            if 'BC' in allowed_op and user._identifier is 'TAG':
                allowed_op.remove('BC')
        op = random.choice(allowed_op)
        print user, op
        thisop         = Operation(allowed_op, op, user, self._userlist)
        user           = thisop._user
        self._userlist = thisop._userlist
        print user, 'after', op, '\n----------'
        return thisop, user


class Identity:
    phoneprefix     = {'TW':'09'}
    defaultCards    = {1:'x',2:'0x',3:'0x1',4:'0x12',5:'0x123',6:'0x1234',\
                       7:'0x12345',8:'0x123456'}


    def __init__(self, identifier='PHONE', size=8,location='TW', mode='default',\
                 value="", point=0):
        """
        >>> me = Identity('PHONE', 1,'TW', 'default')
        >>> me.born()
        >>> me
        PHONE 1 default TW
        0900000001-None

        >>> girl = Identity('PHONE', 2, 'TW', 'default')
        >>> girl.born()
        >>> girl
        PHONE 2 default TW
        0900000011-None

        >>> me.bind(girl)
        Binding Failed Due to Identical Type

        >>> yui = Identity('TAG', 4, 'TW', 'default')
        >>> yui.born()
        >>> me.bind(yui)
        >>> me
        bind(0900000001,0x12)

        """
        self._identifier = identifier
        self._size       = size
        self._mode       = mode
        self._value      = value
        self._location   = location
        self._binding    = None
        self._point      = point

    def __repr__(self):
        if self._binding is not None:
            return '%s %s %s %s\n%s-%s'\
                    %(self._identifier, self._size,self._mode,\
                    self._location,self._value, self._binding._value)
        else:
            return  '%s %s %s %s\n%s-%s'\
                    %(self._identifier, self._size,self._mode,\
                    self._location,self._value, self._binding)


    def id_generator(self, size=6, chars=string.ascii_uppercase +\
                     string.ascii_lowercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    def born(self):
        '''Generate self._value according to configs'''
        if self._identifier is 'PHONE':
            #Generate a phone number as identity value with 090*$(size)
            if self._mode is 'default':
                if len(str(self._size)) <= 8:
                    self._value = Identity.phoneprefix[self._location] \
                                  + str(0)*(8-self._size)\
                                  + str(1)*(self._size)
                else:
                    self._value = Identity.phoneprefix[self._location]+ str(0)*8
            #Generate random phone number
            elif self._mode is 'rand':
                size = 0
                if self._size <= 8:
                    size = self._size
                else:
                    size = 8
                randterm = random.randint(0,10**size-1)
                self._value = Identity.phoneprefix[self._location] \
                              + str(0)*(8-len(str(randterm))) \
                              + str(randterm)
        elif self._identifier is 'TAG':
            if self._mode is 'default':
                if self._size in Identity.defaultCards.keys():
                    self._value = Identity.defaultCards[self._size]
                else:
                    self._value = Identity.defaultCards[8]
            elif self._mode is 'rand':
                self._value = self.id_generator(self._size)

    def bind(self, wife):
        '''Bind other identity'''
        if self._identifier == wife._identifier:
            print 'Binding Failed Due to Identical Type'
            return
        elif self._binding is not None:
            print 'Binding Failed Due to Re-Marriage!!!'
            return
        else:
            print 'bind(%s,%s)' %(self._value, wife._value)
            self._binding = wife
            wife._binding = self

class Operation:

    '''Common Class Attributes'''
    abbr = {'AP':'AddPoint', 'BP':'BindPhone'  , 'BC':'BindCard' , 'RM':'Delete',\
            'RD':'Redeem'  , 'UP':'UnBindPhone', 'UC':'UnBindCard', 'GP':'GetPass'}
    MIN_ADDPOINT = 1
    MAX_ADDPOINT = 20

    def __init__(self, allowed_type, content_type=None, user=None, userlist=None):
        self._allowed_type = dict((k,Operation.abbr[k]) \
                            for k in allowed_type if k in Operation.abbr)
        self._content = None
        if content_type in self._allowed_type.keys():
            self._content_type = self._allowed_type[content_type]
        elif content_type in self._allowed_type.values():
            self._content_type = content_type
        else:
            self._content_type = None
        print content_type
        '''Assign action according to content_type'''
        if content_type is 'AP':
            print '--Add Point--'
            self._content = random.randint(Operation.MIN_ADDPOINT,\
                                           Operation.MAX_ADDPOINT)
            user._point += self._content
        elif content_type == 'RD':
            print '--Redeem Point--'
            self._content = random.randint(0, user._point)
            user._point -= self._content
        elif content_type == 'BC':
            goodgirls = ([u for u in userlist.values()\
                    if u._identifier is 'TAG' and u._binding is None])
            if len(goodgirls) != 0:
                goodgirl = random.choice(goodgirls)
            else:
                print '--Bind failed due to empty choice on TAG'
            self._content = goodgirl._value
            print '--Bind Card--', self._content
            user.bind(userlist[self._content])

        elif content_type is 'BP':
            '''Bind PHONE to an existing card'''
            goodgirls = random.choice([u for u in userlist.values()\
                    if u._identifier is 'PHONE' and u._binding is None])
            if len(goodgirls) != 0:
                goodgirl = random.choice(goodgirls)
            else:
                print '--Bind failed due to empty choice on PHONE'
            self._content = goodgirl._value
            print '--Bind Phone--', self._content
            user.bind(userlist[self._content])

        else:
            print 'what the hell?'

        #use these item below to update parent objects
        self._user     = user
        self._userlist = userlist 

    def __repr__(self):
        return '%s %s' %(self._content_type, self._content)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
