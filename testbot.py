import argparse

from collections import defaultdict


def main():
    from slotter import Slotter
    parser = init_parser()
    args   = get_args(parser)
    
    '''Configuration vars'''
    testfile   = 'testcase.out'
    userfile   = 'userstat.out'
    slot_size  = 100
    phone_user = 50
    tag_user   = 50


    '''-gt TESTFILE = generate testcase'''
    if args['gt']:
        testfile   = args['gt'][0]
    '''-gu USERFILE = generate userstat''' 
    if args['gu']:
        userfile   = args['gu'][0]
    '''-s SIZE      = Set the size of slots'''
    if args['s']:
        slot_size  = int(args['s'][0])
    '''-sp SIZE     = Set the size of PHONE user'''
    if args['sp']:
        phone_user = int(args['sp'][0])
    '''-st SIZE     = Set the size of TAG user'''
    if args['st']:
        tag_user   = int(args['st'][0])

    slotmachine = Slotter(size_phone_user=phone_user, size_tag_user=tag_user,\
                          location='TW',mode='rand',\
                          allowed_operation=('AP','RD'),\
                          test_case_file=testfile,user_stat_file=userfile)
    slotmachine.slot(slot_size)
    if args['gt'] or args['gu']:
        slotmachine.export()


def init_parser():
    parser = argparse.ArgumentParser(description='Setup testbot config')
    parser.add_argument('-gt', help='Set export TESTFILE path',\
            nargs=1,metavar='FILENAME')
    parser.add_argument('-gu', help='Set export USERFILE path',\
            nargs=1,metavar='FILENAME')
    parser.add_argument('-s', help='Set slotter size',\
            nargs=1,metavar='SIZE')
    parser.add_argument('-sp', help='Set PHONE user size',\
            nargs=1,metavar='SIZE')
    parser.add_argument('-st', help='Set TAG user size',\
            nargs=1,metavar='SIZE')

    return parser


def get_args(parser):
    args = parser.parse_args()
    return vars(args)

if __name__ == '__main__':
    print 'Hello! this is testbot!'
    main()

